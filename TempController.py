import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import serial
import serial.tools.list_ports
from pathlib import Path
from datetime import datetime
import re
import os, sys

# Main window setup
root = tk.Tk()

# Global text variables
currenttemp_text = [tk.StringVar(), tk.StringVar()]
targettemp_text = [tk.StringVar(), tk.StringVar()]
serial_port = tk.StringVar()
connect_status = tk.StringVar()
connect_button_text = tk.StringVar()
folder_path = tk.StringVar()
log_button_text = tk.StringVar()
logging_status = tk.StringVar()
logged_datapoints_text = tk.StringVar()
gcode_out_text = tk.StringVar()

currenttemp_text[0].set("0,00 °C")
currenttemp_text[1].set("0,00 °C")
targettemp_text[0].set("0,00")
targettemp_text[1].set("0,00")
serial_port.set("")
connect_button_text.set("Connect")
connect_status.set("No\nconnection")
folder_path.set("C:/")
log_button_text.set("Start logging")
logging_status.set("Temperature log\n- inactive -")
logged_datapoints_text.set("Logged datapoints:\n0")

# Temperature data
currenttemp = [0, 0]

# Status flags
active_connection = False
active_logging = False
logged_datapoints = 0
plot_starttime = datetime.now()
log_starttime = datetime.now()

# Plot settings
plot_timeframe_var = tk.IntVar()            # display the last x seconds in temperature plot
plot_timeframe_var.set(120)
plot_ymin = tk.IntVar()
plot_ymin.set(0)
plot_ymax = tk.IntVar()
plot_ymax.set(100)
plot_timeframe = plot_timeframe_var.get()
plot_channel1 = tk.IntVar()
plot_channel1.set(1)
plot_channel2 = tk.IntVar()
plot_channel2.set(1)
plot_autoscale = tk.IntVar()
plot_autoscale.set(0)

# Log settings
logfile_name = "temp.csv"
logfile = None
start_log_at_zero = tk.IntVar()
start_log_at_zero.set(1)

# Plot data
channel1_plot_data = []
channel2_plot_data = []
time_plot_data = []

# Serial connection handle
ser = serial.Serial(baudrate = 19200, bytesize = 8, parity = 'N', stopbits = 1, timeout = 5)

# Regex for extracting temperature responses
temp_regex = re.compile("T([0-9]):\s+(-?[0-9]+\.[0-9]+)\s\/\s+(-?[0-9]+\.[0-9]+)")


def update_timeframe():
    global plot_timeframe
    try:
        plot_timeframe = plot_timeframe_var.get()
        update_plot(add_data = False)
    except:
        pass

# Clear all plotted data
def reset_plot():
    global plot_starttime
    global channel1_plot_data, channel2_plot_data, time_plot_data

    # clear plot data
    channel1_plot_data = []
    channel2_plot_data = []
    time_plot_data = []
    
    update_plot(add_data = False)


def update_plot(add_data):
    global plot_starttime
    global currenttemp
    global channel1_plot_en, channel2_plot_en
    global channel1_plot_data, channel2_plot_data, time_plot_data
    global ax, fig, plot_canvas

    # if add_data == False, the plot only gets redrawn without adding data
    if add_data:

        # First datapoint always at time = 0s
        if len(time_plot_data) == 0:
            plot_starttime = datetime.now()

        # add most recent datapoints to plot data
        channel1_plot_data.append(currenttemp[0])
        channel2_plot_data.append(currenttemp[1])
        time_plot_data.append((datetime.now() - plot_starttime).total_seconds())
        
        # Remove all datapoints that are older than selected timeframe
        # Keep first datapoint outside canvas area, so the line to first datapoint inside canvas area is still drawn correctly
        if time_plot_data[-1] > plot_timeframe:
            while len(time_plot_data) > 2 and time_plot_data[1] < (time_plot_data[-1] - plot_timeframe):
                channel1_plot_data.pop(0)
                channel2_plot_data.pop(0)
                time_plot_data.pop(0)

        #print("Plotted %s, %s at time %s" % (channel1_plot_data[-1], channel2_plot_data[-1], time_plot_data[-1],))
        #print("Only keeping %s datapoints" % len(time_plot_data))

    ax.clear()
    ax.set(xlabel = 'Time [s]', ylabel = 'Temperature [°C]')
    ax.grid()
    ax.set_xlim(0, plot_timeframe)
    if not plot_autoscale.get():
        try:
            ax.set_ylim(plot_ymin.get(), plot_ymax.get())
        except:
            pass
    # only plot if data is available
    if len(time_plot_data) > 0:
        if time_plot_data[-1] > plot_timeframe:
            ax.set_xlim(time_plot_data[-1] - plot_timeframe, time_plot_data[-1])
        if plot_channel1.get():
            ax.plot(time_plot_data, channel1_plot_data, label = "Channel 1")
        if plot_channel2.get():
            ax.plot(time_plot_data, channel2_plot_data, label = "Channel 2")
        if plot_channel1.get() or plot_channel2.get():
            ax.legend(loc = 'upper left')
        
    # redraw Figure element
    plot_canvas.draw()


# Check serial connection for new line of response
def receive_data():
    global active_connection
    global ser

    if active_connection:
        while ser.in_waiting > 0:
            parse_data(ser.readline())
        
        # call periodically as long as connection is active
        root.after(100, receive_data)

# Scan provided byte array for temperature report data
def parse_data(response):
    global gcode_in
    global currenttemp, currenttemp_text

    channel2_data_decoded = False
    gcode_in.insert('end', response)

    # Try to extract temperature data from response
    # Valid Temperature reports look like this: "Tx: yy.yy/ zz.zz\n"
    # x - channel, y - current temp, z - target temp
    try:        
        res = temp_regex.match(response.decode())
        idx = int(res.group(1))
        currenttemp[idx] = float(res.group(2))
        currenttemp_text[idx].set(("%.2f °C" % currenttemp[idx]).replace(".", ","))
        if idx == 1:
            channel2_data_decoded = True
        # color line green if decoding was successful
        gcode_in.tag_add("success", gcode_in.index("end-2l"), gcode_in.index("end-2l lineend"))

    except:
        if (b'error') in response:
            gcode_in.tag_add("error", gcode_in.index("end-2l"), gcode_in.index("end-2l lineend"))

    # Temperature data always gets sent channel 1 first, channel 2 second. 
    # Receiving channel 2 data completes a data frame -> update plot and log to file
    if channel2_data_decoded:
        update_plot(add_data = True)
        add_logentry()    
    
    gcode_in.see('end')

# Write current temperatures to currently opened logfile
def add_logentry():
    global active_logging
    global start_log_at_zero
    global log_starttime
    global logged_datapoints, logged_datapoints_text
    global logfile

    if not active_logging:
        return
    
    # Reset logging timer if first datapoint is stored
    if logged_datapoints == 0 and start_log_at_zero.get():
        log_starttime = datetime.now()

    try:
        diff_sec = (datetime.now() - log_starttime).total_seconds()
        logline = "%.2f;%02d:%02d;%.2f;%.2f\n" % (diff_sec, int(diff_sec / 60), round(diff_sec % 60), currenttemp[0], currenttemp[1])
        logline = logline.replace(".", ",")
        logfile.write(logline)
        logged_datapoints = logged_datapoints + 1
        logged_datapoints_text.set("Logged datapoints:\n%s" % logged_datapoints)
    except:
        # error -> end logging by simulating button press
        # log_button_cb()
        logging_status.set("Temperature log\n- Error -")
        log_status_label.config(fg = "red")
    
# General function to send out gcode
def send_gcode(gcode):
    global gcode_out
    global ser
    
    #messagebox.showinfo("Sending Gcode", gcode)
    try:
        res = ser.write(gcode.encode('utf-8') + b'\n')
        #messagebox.showinfo("Success", "Sent %s bytes" % res)
        gcode_out.insert('end', gcode + "\n")
        gcode_out.see('end')
    except:
        messagebox.showerror("Error", "Tried to send\n" + gcode)

# General function to set target temperature
def set_temperature(channel):
    global gcode_out
    global targettemp_text
    global ser

    if channel not in [0, 1]:
        return

    gcode = "M104 T%s S%s" % (channel, targettemp_text[channel].get().replace(",", "."))
    send_gcode(gcode)

# Button callback to send Gcode
def send_button_cb():
    global gcode_out_text

    send_gcode(gcode_out_text.get())
    gcode_out_text.set("")

# Button callback to select logging folder
def browse_button_cb():
    global folder_path

    filename = tk.filedialog.askdirectory()
    folder_path.set(filename)
    print(filename)

# Button callback to start / stop logging
def log_button_cb():
    global active_logging
    global logging_status, log_button_text
    global logfile_name, folder_path, logfile
    global log_starttime, logged_datapoints

    if active_logging:
        try:
            logfile.close()
            messagebox.showinfo("Logfile saved", "Temperature log was saved as\n%s" % logfile_name)
        except:
            messagebox.showerror("Logging error", "Logfile could not be saved. Maybe check at\n %s" % logfile_name)

        log_button_text.set("Start logging")
        logging_status.set("Temperature log\n- inactive -")
        log_status_label.config(fg = "black")
        active_logging = False
        return

    try:
        path = Path(folder_path.get())
        if not path.exists() or not path.is_dir():
            messagebox.showerror("Logging folder", "Selected folder is not a directory or does not exist.")
            return
        logfile_name = datetime.now().strftime("%Y-%m-%d_%H-%M-%S_temperatures.csv")
        path = path / logfile_name
        logfile = open(path, "w+")
        logfile.write("Time [s];Time[mm:ss];Channel 1 [°C];Channel 2 [°C]\n")

        log_starttime = datetime.now()
        logged_datapoints = 0

        # Success -> update GUI
        log_button_text.set("Stop logging")
        logging_status.set("Temperature log\n- active -")
        log_status_label.config(fg = "black")
        active_logging = True
    except:
        messagebox.showerror("Logging error", "Could not start temperature log\nDo you have permissions for selected folder?")

# Button callback to scan for serial ports
def scan_button_cb():
    global serial_port_select

    serial_port_select['values'] = ()
    try:
        for comport in serial.tools.list_ports.comports():
            serial_port_select['values'] = serial_port_select['values'] + (comport.device)
    except:
        pass

    if len(serial_port_select['values']) > 0:
        serial_port_select.current(0)

# General function to disable / enable widgets based on connection status
def set_gui_state(connection_active):
    global channel1_button
    global channel2_button
    global send_button
    global serial_port_select
    global ser

    channel1_button['state'] = 'normal' if connection_active else 'disabled'
    channel2_button['state'] = 'normal' if connection_active else 'disabled'
    send_button['state'] =  'normal' if connection_active else 'disabled'

    serial_port_select['state'] = 'disabled' if connection_active else 'normal'
    
    connect_button_text.set("Disconnect" if connection_active else "Connect")
    connect_status.set("Connected to\n" + ser.port if connection_active else "No\nconnection")

# Function to establish a connection
def connect_button_cb():
    global active_connection
    global connect_button_text
    global connect_status
    global serial_port_select

    if active_connection:
        ser.close()
        active_connection = False
        set_gui_state(connection_active = False)
        return
    
    try:
        ser.port = serial_port.get()
        ser.open()
        ser.setDTR(True)
        active_connection = True
        set_gui_state(connection_active = True)
        root.after(100, receive_data)
        reset_plot()
        #send_gcode("M17 T0")      # immediately activate the first two temperature channels
        #send_gcode("M17 T1")
        return
    
    except:
        connect_status.set("Connection\nerror")
        return


def autoscale_cb():
    if plot_autoscale.get():
        ymax_entry['state'] = 'disabled'
        ymin_entry['state'] = 'disabled'
    else:
        ymax_entry['state'] = 'normal'
        ymin_entry['state'] = 'normal'
    update_plot(add_data = False)


# Main window config
#root.geometry('+1920+0')        # move window to second monitor
root.state('zoomed')            # and maximize window there
root.iconbitmap(os.path.join(getattr(sys, '_MEIPASS', os.getcwd()), "TempController.ico"))
root.title("Temperature Controller")
root.configure(bg = 'white')
root.minsize(1000, 750)
root.grid_columnconfigure((1, 2), weight = 1)
root.grid_rowconfigure(0, weight = 1)


# Border around temperatures
temperatures_frame = tk.LabelFrame(root, text = " Temperatures ", bd = 3, relief = 'sunken', bg = 'white')
temperatures_frame.grid(row = 0, column = 0, columnspan = 3, padx = 100, pady = (40, 10), sticky = 'news')
temperatures_frame.grid_columnconfigure(1, weight = 1)
temperatures_frame.grid_rowconfigure(2, weight = 1)

# Inputs for min and max temperature
ymax_entry = tk.Entry(temperatures_frame, width = 5, justify = 'right', textvariable = plot_ymax)
ymax_entry.grid(row = 0, column = 0, sticky = "n", padx = (10, 0), pady = (20, 0))
ymax_entry.bind('<Return>', lambda _: update_plot(add_data = False))
ymin_entry = tk.Entry(temperatures_frame, width = 5, justify = 'right', textvariable = plot_ymin)
ymin_entry.grid(row = 2, column = 0, sticky = "s", padx = (10, 0), pady = (0, 50))
ymin_entry.bind('<Return>', lambda _: update_plot(add_data = False))

# Combined temperature graph
fig, ax = plt.subplots(figsize = (3, 4), dpi = 100, tight_layout = True)
# Closing pyplot immediately prevents the python process from never ending after closing the program
# Drawing to the created subplot still works fine after this
plt.close()
# Add graph to layout
plot_canvas = FigureCanvasTkAgg(fig, temperatures_frame)
plot_canvas.get_tk_widget().grid(row = 0, column = 1, rowspan = 3, sticky = 'news', padx = (0, 5), pady = 5)
# draw plot for the first time
update_plot(add_data = False)


# Border around controls for channel 1
channel1_controls = tk.LabelFrame(temperatures_frame, text = ' Channel 1 ', bd = 3, relief = 'ridge', bg = 'white', width = "250px")
channel1_controls.grid(row = 0, column = 2, sticky = 'new', padx = 10, pady = 10)
channel1_controls.grid_columnconfigure((0, 1), weight = 1)
# Label for current temperature for channel 1
tk.Label(channel1_controls, text = "Current temperature:", justify = 'left', bg = 'white').grid(row = 0, column = 0, padx = 5, pady = 5, sticky = 'w')
tk.Label(channel1_controls, bg = 'white', textvariable = currenttemp_text[0], justify = 'left').grid(row = 0, column = 1, padx = 5, pady = 5, sticky = 'e')
# Label and Entry box for target temperature for channel 1
tk.Label(channel1_controls, text = "Target temperature:", justify = 'left', bg = 'white').grid(row = 1, column = 0, padx = 5, pady = 5, sticky = 'w')
temp_input_box1 = tk.Frame(channel1_controls)
temp_input_box1.grid(row = 1, column = 1, padx = 5, pady = 5, sticky = 'e')
target1_entry = tk.Entry(temp_input_box1, textvariable = targettemp_text[0], width = 5, justify = 'right')
target1_entry.pack(side = 'left')
target1_entry.bind('<Return>', lambda _: set_temperature(0))
tk.Label(temp_input_box1, text = " °C", bg = 'white').pack(side = 'left')
# Button to send target temperature for channel 1
channel1_button = tk.Button(channel1_controls, text = "Set temperature", command = lambda: set_temperature(0))
channel1_button.grid(row = 2, column = 0, padx = 5, pady = 5)
# Checkbox to enable channel 1 plotting
channel1_plot_en = tk.Checkbutton(channel1_controls, text = "Enable\nplot", variable = plot_channel1, bg = 'white', command = lambda: update_plot(add_data = False))
channel1_plot_en.grid(row = 2, column = 1, padx = 5, pady = 5)
channel1_button['state'] = 'disabled'


# Border around controls for channel 2
channel2_controls = tk.LabelFrame(temperatures_frame, text = ' Channel 2 ', bd = 3, relief = 'ridge', bg = 'white', width = "250px")
channel2_controls.grid(row = 1, column = 2, sticky = 'new', padx = 10, pady = 10)
channel2_controls.grid_columnconfigure((0, 1), weight = 1)
# Label for current temperature for channel 2
tk.Label(channel2_controls, text = "Current temperature:", justify = 'left', bg = 'white').grid(row = 0, column = 0, padx = 5, pady = 5, sticky = 'w')
tk.Label(channel2_controls, bg = 'white', textvariable = currenttemp_text[1], justify = 'left').grid(row = 0, column = 1, padx = 5, pady = 5, sticky = 'e')
# Label and Entry box for target temperature for channel 2
tk.Label(channel2_controls, text = "Target temperature:", justify = 'left', bg = 'white').grid(row = 1, column = 0, padx = 5, pady = 5, sticky = 'w')
temp_input_box2 = tk.Frame(channel2_controls)
temp_input_box2.grid(row = 1, column = 1, padx = 5, pady = 5, sticky = 'e')
target2_entry = tk.Entry(temp_input_box2, textvariable = targettemp_text[1], width = 5, justify = 'right')
target2_entry.pack(side = 'left')
target2_entry.bind('<Return>', lambda _: set_temperature(1))
tk.Label(temp_input_box2, text = " °C", bg = 'white').pack(side = 'left')
# Button to send target temperature for channel 2
channel2_button = tk.Button(channel2_controls, text = "Set temperature", command = lambda: set_temperature(1))
channel2_button.grid(row = 2, column = 0, padx = 5, pady = 5)
# Checkbox to enable channel 2 plotting
channel2_plot_en = tk.Checkbutton(channel2_controls, text = "Enable\nplot", variable = plot_channel2, bg = 'white', command = lambda: update_plot(add_data = False))
channel2_plot_en.grid(row = 2, column = 1, padx = 5, pady = 5)
channel2_button['state'] = 'disabled'


# Border around controls
plot_controls = tk.LabelFrame(temperatures_frame, text = ' Controls ', bd = 3, relief = 'ridge', bg = 'white', width = "250px")
plot_controls.grid(row = 2, column = 2, sticky = 'new', padx = 10, pady = 10)
plot_controls.grid_columnconfigure((0, 1), weight = 1)
# Label and Entry box for timeframe
tk.Label(plot_controls, text = "Plot timeframe:", justify = 'left', bg = 'white').grid(row = 0, column = 0, padx = 5, pady = 5, sticky = 'w')
timeframe_input_box = tk.Frame(plot_controls)
timeframe_input_box.grid(row = 0, column = 1, padx = 5, pady = 5, sticky = 'e')
timeframe_entry = tk.Entry(timeframe_input_box, textvariable = plot_timeframe_var, width = 5, justify = 'right')
timeframe_entry.pack(side = 'left')
timeframe_entry.bind('<Return>', lambda _: update_timeframe())
tk.Label(timeframe_input_box, text = " seconds", bg = 'white').pack(side = 'left')
# Button to clear plot
plot_clear_button = tk.Button(plot_controls, text = "Clear plot", command = reset_plot)
plot_clear_button.grid(row = 1, column = 0, padx = 5, pady = 5)
# Checkbox to enable auto y axis scale
tk.Checkbutton(plot_controls, text = "Auto\nscale plot", variable = plot_autoscale, bg = 'white', command = autoscale_cb).grid(row = 1, column = 1, padx = 5, pady = 5, sticky = "e")
# Channel selection
channel_select_frame = tk.Frame(plot_controls, bg = 'white')
channel_select_frame.grid(row = 2, column = 0, columnspan = 2)
channel_select_frame.grid_columnconfigure((1, 2, 3, 4), weight = 1)
tk.Label(channel_select_frame, text = "Channels:", bg = 'white').grid(row = 0, column = 0)
tk.Checkbutton(channel_select_frame, text = "1", bg = 'white', ).grid(row = 0, column = 1)
tk.Checkbutton(channel_select_frame, text = "2", bg = 'white', ).grid(row = 0, column = 2)
tk.Checkbutton(channel_select_frame, text = "3", bg = 'white', ).grid(row = 0, column = 3)
tk.Checkbutton(channel_select_frame, text = "4", bg = 'white', ).grid(row = 0, column = 4)


# Border around connection
connection_frame = tk.LabelFrame(root, text = " Connection ", bd = 3, relief = 'sunken', bg = 'white')
connection_frame.grid(row = 1, column = 0, padx = (100, 10), pady = 10, sticky = 'news')
connection_frame.grid_rowconfigure(0, weight = 1)
connection_frame.grid_columnconfigure(3, weight = 1)
connection_frame.grid_columnconfigure(4, weight = 2)
# Serial port selection
tk.Label(connection_frame, text = "Enter serial port:", bg = 'white').grid(row = 0, column = 0, padx = 5, pady = 5)
serial_port_select = ttk.Combobox(connection_frame, textvariable = serial_port, width = 10, values = "")
serial_port_select.grid(row = 0, column = 1, padx = 5, pady = 5)
# Scan button
tk.Button(connection_frame, text = "Scan", command = scan_button_cb).grid(row = 0, column = 2, padx = 10, pady = 5, sticky = 'ew')
# Connect button
tk.Button(connection_frame, textvariable = connect_button_text, command = connect_button_cb).grid(row = 0, column = 3, padx = 10, pady = 5, sticky = 'ew')
# Connection status
tk.Label(connection_frame, textvariable = connect_status, bg = 'white').grid(row = 0, column = 4, padx = 10, pady = 5, sticky = 'ew')


# Border around logging
logging_frame = tk.LabelFrame(root, text = " Logging ", bd = 3, relief = 'sunken', bg = 'white')
logging_frame.grid(row = 2, column = 0, padx = (100, 10), pady = (10, 40), sticky = 'news')
logging_frame.grid_rowconfigure((0,1), weight = 1)
# Current log location
tk.Label(logging_frame, text = "Folder for logfiles:", bg = 'white').grid(row = 0, column = 0, padx = 5, pady = 5)
tk.Entry(logging_frame, textvariable = folder_path, width = 40).grid(row = 0, column = 1, columnspan = 2, padx = 5, pady = 5)
# Browse button
tk.Button(logging_frame, text = "Browse", command = browse_button_cb).grid(row = 0, column = 3, padx = 10, pady = 5, sticky = 'ew')
# Logging button
tk.Button(logging_frame, textvariable = log_button_text, command = log_button_cb).grid(row = 1, column = 0, padx = 10, pady = (20, 10), sticky = 'ew')
# Start at zero checkbox
tk.Checkbutton(logging_frame, text = "Start log\nat 00:00", variable = start_log_at_zero, bg = 'white').grid(row = 1, column = 1, padx = 10, pady = (20, 10))
# Logging datapoints
tk.Label(logging_frame, textvariable = logged_datapoints_text, bg = 'white').grid(row = 1, column = 2, padx = 10, pady = (20, 10))
# Logging status
log_status_label = tk.Label(logging_frame, textvariable = logging_status, bg = 'white')
log_status_label.grid(row = 1, column = 3, padx = 10, pady = (20, 10))


# Gcode input
gcode_in_frame = tk.LabelFrame(root, text = " Incoming Gcode ", bd = 3, relief = 'sunken', bg = 'white')
gcode_in_frame.grid(row = 1, rowspan = 2, column = 1, padx = 10, pady = (10, 40), sticky = 'nsew')
gcode_in = tk.Text(gcode_in_frame, width = 40, height = 12, bg = '#2b2b2b', fg = 'white', bd = 2, relief = 'sunken', highlightbackground = '#242424')
gcode_in.pack(expand = True, fill = 'both', padx = 5, pady = 5)
gcode_in.tag_config("success", foreground = "green")
gcode_in.tag_config("error", foreground = "red")
#gcode_in.insert('end', "Dummy text.")


# Gcode output
gcode_out_frame = tk.LabelFrame(root, text = " Outgoing Gcode ", bd = 3, relief = 'sunken', bg = 'white')
gcode_out_frame.grid(row = 1, rowspan = 2, column = 2, padx = (10, 100), pady = (10, 40), sticky = 'nsew')
gcode_out = tk.Text(gcode_out_frame, width = 40, height = 10, bg = '#2b2b2b', fg = 'white', bd = 2, relief = 'sunken', highlightbackground = '#242424')
gcode_out.pack(side = 'top', expand = True, fill = 'both', padx = 5, pady = 5)
#gcode_out.insert('end', "Dummy text.")
gcode_entry = tk.Entry(gcode_out_frame, textvariable = gcode_out_text, width = 10, bg = '#2b2b2b', fg = 'white', justify = 'left')
gcode_entry.pack(side = 'left', expand = True, fill = 'x', padx = 5, pady = 5)
gcode_entry.bind('<Return>', lambda _: send_button_cb())
send_button = tk.Button(gcode_out_frame, text = "     Send     ", command = send_button_cb, state = 'disabled')
send_button.pack(side = 'left', padx = 5, pady = 5)

#debug_button = tk.Button(root, text = "Debug", command = lambda: parse_data("dummy data \n"))
#debug_button.grid(row = 3, column = 0, padx = (100, 10), pady = 10, sticky = 'nsew')


# ===== Entry point for main application =====

# Scan serial devices right at the start
scan_button_cb()

# Tkinter infinite loop
root.mainloop()